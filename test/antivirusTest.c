/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "jni_test.h"
#include "antivirus.c"

World internet;
Actor antivirus;

void setUp() {
	// Given a lymph fluid with a antivirus
	internet = newWorld("internet");
	int actorsNumber;
	Actor* actors = getActors(internet, "antivirus", &actorsNumber);
	assertEquals_int(1, actorsNumber);
	antivirus = actors[0];
}

void testGetX() {
	// Given
	setUp();

	// When
	int x = getX(antivirus);

	// Then the antivirus is still in the same place
	assertEquals_int(231, x);
}

void testGetY() {
	// Given
	setUp();

	// When
	int y = getY(antivirus);

	// Then the antivirus is still in the same place
	assertEquals_int(203, y);
}

void testGetRotation() {
	// Given
	setUp();

	// When nothing
	int rotation = getRotation(antivirus);

	// Then the antivirus is still in the same direction
	assertEquals_int(0, rotation);
}

void testGetImage() {
	// Given
	setUp();

	// When
	const char* imageFile = getImageFile(antivirus);

	// Then the antivirus has its initial image
	assertEquals_String("antivirus.png", imageFile);
}

void testMove() {
	// Given the initial position
	setUp();
	int x = getX(antivirus);
	int y = getY(antivirus);

	// When any key is not typed
	actAntivirus(antivirus);

	// Then the antivirus moves 5
	assertEquals_int(x+5, getX(antivirus));
	assertEquals_int(  y, getY(antivirus));
}

void testMoveBis() {
	// Given the initial position
	setUp();
	int x = getX(antivirus);
	int y = getY(antivirus);

	// When any key is not typed
	runOnceWorld(internet);

	// Then the antivirus moves 5
	assertEquals_int(x+5, getX(antivirus));
	assertEquals_int(  y, getY(antivirus));
}

void testMoveBisBis() {
	// Given the initial position
	setUp();
	int x = getX(antivirus);
	int y = getY(antivirus);

	// When any key is not typed
	runOnceActors(&antivirus,1);

	// Then the antivirus moves 5
	assertEquals_int(x+5, getX(antivirus));
	assertEquals_int(  y, getY(antivirus));
}

void testTurnRight() {
	// Given the initial position
	setUp();
	assertEquals_int(0, getRotation(antivirus));

	// When right key is typed
	typeKey("right");
	actAntivirus(antivirus);

	// Then the antivirus rotates 4 degrees to the right
	assertEquals_int(4, getRotation(antivirus));
}

void testTurnRightBis() {
	// Given the initial position
	setUp();
	int degrees = getRotation(antivirus);

	// When right key is typed
	typeKey("right");
	actAntivirus(antivirus);

	// Then the antivirus rotates 4 degrees to the right
	assertEquals_int((360+degrees+4)%360, getRotation(antivirus));
}

void testTurnRightBisBis() {
	// Given the initial position
	setUp();
	setRotation(antivirus, 10);

	// When right key is typed
	typeKey("right");
	actAntivirus(antivirus);

	// Then the antivirus rotates 4 degrees to the right
	assertEquals_int(14, getRotation(antivirus));
}

void testTurnLeft() {
	// Given the initial position
	setUp();
	assertEquals_int(0, getRotation(antivirus));

	// When left key is typed
	typeKey("left");
	actAntivirus(antivirus);

	// Then the antivirus rotates 4 degrees to the left
	assertEquals_int(356, getRotation(antivirus));
}

void testTurnLeftBis() {
	// Given the initial position
	setUp();
	int degrees = getRotation(antivirus);

	// When left key is typed
	typeKey("left");
	actAntivirus(antivirus);

	// Then the antivirus rotates 4 degrees to the left
	assertEquals_int((360+degrees-4)%360, getRotation(antivirus));
}

void testTurnLeftBisBis() {
	// Given the initial position
	setUp();
	setRotation(antivirus, 10);

	// When left key is typed
	typeKey("left");
	actAntivirus(antivirus);

	// Then the antivirus rotates 4 degrees to the left
	assertEquals_int(6, getRotation(antivirus));
}

void testTurnRight90() {
	// Given a antivirus that has rotated 90 degrees to the right
	setUp();
	turn(antivirus, 90);

	// When any key is not typed
	int x = getX(antivirus);
	int y = getY(antivirus);
	actAntivirus(antivirus);

	// Then the antivirus goes down 5
	assertEquals_int(  x, getX(antivirus));
	assertEquals_int(y+5, getY(antivirus));
}

void testTurnRight180() {
	// Given a antivirus that has rotated 180 degrees to the right
	setUp();
	turn(antivirus, 180);

	// When any key is not typed
	int x = getX(antivirus);
	int y = getY(antivirus);
	actAntivirus(antivirus);

	// Then the antivirus goes back 5
	assertEquals_int(x-5, getX(antivirus));
	assertEquals_int(  y, getY(antivirus));
}

void testTurnRight270() {
	// Given a antivirus that has rotated 270 degrees to the right
	setUp();
	turn(antivirus, 270);

	// When any key is not typed
	int x = getX(antivirus);
	int y = getY(antivirus);
	actAntivirus(antivirus);

	// Then the antivirus goes up 5
	assertEquals_int(  x, getX(antivirus));
	assertEquals_int(y-5, getY(antivirus));
}

void testTurnLeft90() {
	// Given a antivirus that has rotated 90 degrees to the left
	setUp();
	turn(antivirus, -90);

	// When any key is not typed
	int x = getX(antivirus);
	int y = getY(antivirus);
	actAntivirus(antivirus);

	// Then the antivirus goes up 5
	assertEquals_int(  x, getX(antivirus));
	assertEquals_int(y-5, getY(antivirus));
}

void testTurnLeft180() {
	// Given a antivirus that has rotated 180 degrees to the left
	setUp();
	turn(antivirus, -180);

	// When any key is not typed
	int x = getX(antivirus);
	int y = getY(antivirus);
	actAntivirus(antivirus);

	// Then the antivirus goes back 5
	assertEquals_int(x-5, getX(antivirus));
	assertEquals_int(  y, getY(antivirus));
}

void testTurnLeft270() {
	// Given a antivirus that has rotated 270 degrees to the left
	setUp();
	turn(antivirus, -270);

	// When any key is not typed
	int x = getX(antivirus);
	int y = getY(antivirus);
	actAntivirus(antivirus);

	// Then the antivirus goes down 5
	assertEquals_int(  x, getX(antivirus));
	assertEquals_int(y+5, getY(antivirus));
}

void testLookForVirus() {
	// Given a virus in the same position as the antivirus
	setUp();
	assertEquals_int(3, getActorsNumber(internet, "virus"));
	int actorsNumber;
	Actor virus = getActors(internet, "virus", &actorsNumber)[0];
	setLocation(virus, getX(antivirus), getY(antivirus));

	// When the antivirus checks that there is a virus
	actAntivirus(antivirus);

	// Then the virus's complaint can be listened when the antivirus eats it
	assertEquals_String("au.wav", getPlayedSound());
	assertEquals_int(2, getActorsNumber(internet, "virus"));
}

void testLookForVirusBis() {
	// Given a virus in the same position as the antivirus
	setUp();
	Actor virus = newActor("virus");
	int x = getX(antivirus);
	int y = getY(antivirus);
	addActorToWorld(internet, virus, x, y);

	// When the antivirus checks that there is a virus
	actAntivirus(antivirus);

	// Then the virus's complaint can be listened when the antivirus eats it
	assertEquals_String("au.wav", getPlayedSound());
	assertEquals_int(0, getActorsNumberAt(internet, x, y, "virus"));
}

void testChangeImage() {
	// Given a virus in the same position as the antivirus
	setUp();
	Actor virus = newActor("virus");
	int x = getX(antivirus);
	int y = getY(antivirus);
	addActorToWorld(internet, virus, x, y);

	// When the antivirus checks that there is a virus
	actAntivirus(antivirus);

	// Then the antivirus image is changed to antivirus-1.png
	assertEquals_String("antivirus-1.png", getImageFile(antivirus));
}

void testChangeImageAgain() {
	// Given a virus in the same position as the antivirus
	setUp();
	Actor virus = newActor("virus");
	int x = getX(antivirus);
	int y = getY(antivirus);
	addActorToWorld(internet, virus, x, y);

	// When the antivirus checks that there is a virus
	actAntivirus(antivirus);
	actAntivirus(antivirus);
	actAntivirus(antivirus);
	actAntivirus(antivirus);
	actAntivirus(antivirus);

	// Then the antivirus image is changed to antivirus-2.png after five acts
	assertEquals_String("antivirus-2.png", getImageFile(antivirus));
}

void testChangeImageAgainAgain() {
	// Given a virus in the same position as the antivirus
	setUp();
	Actor virus = newActor("virus");
	int x = getX(antivirus);
	int y = getY(antivirus);
	addActorToWorld(internet, virus, x, y);

	// When the antivirus checks that there is a virus
	actAntivirus(antivirus);
	actAntivirus(antivirus);
	actAntivirus(antivirus);
	actAntivirus(antivirus);
	actAntivirus(antivirus);
	actAntivirus(antivirus);
	actAntivirus(antivirus);
	actAntivirus(antivirus);
	actAntivirus(antivirus);

	// Then the antivirus image is changed to antivirus-2.png after nine acts
	assertEquals_String("antivirus-3.png", getImageFile(antivirus));
}

void testChangeImageAgainAgainAgain() {
	// Given a virus in the same position as the antivirus
	setUp();
	Actor virus = newActor("virus");
	int x = getX(antivirus);
	int y = getY(antivirus);
	addActorToWorld(internet, virus, x, y);

	// When the antivirus checks that there is a virus
	actAntivirus(antivirus);
	actAntivirus(antivirus);
	actAntivirus(antivirus);
	actAntivirus(antivirus);
	actAntivirus(antivirus);
	actAntivirus(antivirus);
	actAntivirus(antivirus);
	actAntivirus(antivirus);
	actAntivirus(antivirus);
	actAntivirus(antivirus);
	actAntivirus(antivirus);
	actAntivirus(antivirus);
	actAntivirus(antivirus);

	// Then the antivirus image is changed to antivirus-2.png after thirteen acts
	assertEquals_String("antivirus.png", getImageFile(antivirus));
}
