/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "jni_test.h"
#include "computer.c"

World internet;
Actor computer;

void setUp() {
	// Given the first of the ten computers
	internet = newWorld("internet");
	int actorsNumber;
	assertEquals_int(10, getActorsNumber(internet, "computer"));
	computer = getActors(internet, "computer", &actorsNumber)[0];
}

void testNoMove() {
	// Given the initial position of the computer
	setUp();
	int x = getX(computer);
	int y = getY(computer);

	for (int k=0; k<20; k++) {
		// When time goes by
		actComputer(computer);

		// Then the computer is still in the same place
		assertEquals_int(x, getX(computer));
		assertEquals_int(y, getY(computer));
	}
}
