/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "jni_test.h"
#include "internet.c"

void testStartGame() {
    // Given
    World internet;

    // When
    internet = newWorld("internet");

    // Then
    assertEquals_int(560, getWidth(internet));
    assertEquals_int(560, getHeight(internet));
    assertEquals_int(  1, getCellSize(internet));
    assertEquals_int(  1, getActorsNumber(internet, "antivirus"));
    assertEquals_int(  3, getActorsNumber(internet, "virus"));
    assertEquals_int( 10, getActorsNumber(internet, "computer"));
}

