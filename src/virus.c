/**
 * This file defines a virus.
 *
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "greenfoot.h"

/**
 * Initialize its image.
 */
void startVirus(Actor virus) {
	setImageFile(virus, "virus.png");
}

/**
 * Check if we are at the edge of the world. If we are on the
 * edge, we turn a little. If not, nothing is done.
 */
void turnAtEdge(Actor virus) {
	if (isAtEdge(virus)) {
		turn(virus, 17);
	}
}

/**
 * It randomly decides to turn from the current direction, or not.
 * If you choose to turn, you turn a little to the left or right
 * by a random number of degrees.
 */
void randomTurn(Actor virus) {
	if (getRandomNumber(100) > 90) {
		turn(virus, getRandomNumber(90)-45);
	}
}

/**
 * Check if we have come across a computer. when it finds a computer, it
 * eats it. If not, it does nothing. When only two computers remain,
 * the game ends.
 */
void lookForComputer(Actor virus) {
	if (isTouching(virus, "computer")) {
		removeTouching(virus, "computer");
		playSound("slurp.wav");
		World internet = getWorld(virus);
		if (getActorsNumber(internet, "computer") == 2) {
			playSound("hooray.wav");
			stopScenario();
		}
	}
}

/**
 * Act - do whatever the virus wants to do. This function
 * is called whenever the 'Act' or 'Run' button gets pressed
 * in the environment.
 */
void actVirus(Actor virus) {
	turnAtEdge(virus);
	randomTurn(virus);
	move(virus, 5);
    lookForComputer(virus);
}

