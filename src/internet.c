/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "greenfoot.h"

/**
 * Define the world size: width, height, and computer size
 */
#define _width    560
#define _height   560
#define _computerSize 1

/**
 * Initialize the background and actors when the game starts:
 * one antivirus, three virus, and ten computers.
 */
void startInternet(World internet) {
	setBackgroundFile(internet, "internet.jpg");

	Actor antivirus = newActor("antivirus");
    addActorToWorld(internet, antivirus, 231, 203);

    Actor virus1 = newActor("virus");
    addActorToWorld(internet, virus1, 334, 65);
    Actor virus2 = newActor("virus");
    addActorToWorld(internet, virus2, 481, 481);
    Actor virus3 = newActor("virus");
    addActorToWorld(internet, virus3, 79, 270);

    Actor computer1  = newActor("computer");
    addActorToWorld(internet, computer1, 445, 137);
    Actor computer2 = newActor("computer");
    addActorToWorld(internet, computer2, 454, 369);
    Actor computer3 = newActor("computer");
    addActorToWorld(internet, computer3, 368, 466);
    Actor computer4 = newActor("computer");
    addActorToWorld(internet, computer4, 129, 488);
    Actor computer5 = newActor("computer");
    addActorToWorld(internet, computer5, 254, 388);
    Actor computer6 = newActor("computer");
    addActorToWorld(internet, computer6, 106, 334);
    Actor computer7 = newActor("computer");
    addActorToWorld(internet, computer7, 338, 112);
    Actor computer8 = newActor("computer");
    addActorToWorld(internet, computer8, 150, 94);
    Actor computer9 = newActor("computer");
    addActorToWorld(internet, computer9, 373, 240);
    Actor computer10 = newActor("computer");
    addActorToWorld(internet, computer10, 509, 55);
}

