/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

#include "computerTest.c"
extern JNIEnv *javaEnv;

JNIEXPORT void JNICALL Java_computerTest_testNoMove
  (JNIEnv *env, jobject object)
{
    javaEnv = env;
    testNoMove();
}

