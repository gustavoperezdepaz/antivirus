/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

public class antivirus extends greenfoot.Actor  {

	public antivirus() {
		start();
	}

    public void start(){
        start_();
    }
    private native void start_();

	public void act(){
        act_();
    }
    private native void act_();


    static {
        System.load(new java.io.File(".jni", "antivirus_jni.so").getAbsolutePath());
    }
}
