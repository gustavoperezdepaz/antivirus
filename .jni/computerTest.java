/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

import org.junit.Test;
import org.junit.runner.RunWith;
import greenfoot.junitUtils.runner.GreenfootRunner;
@RunWith(GreenfootRunner.class)
public class computerTest {
    @Test
    public native void testNoMove();

    static {
        System.load(new java.io.File(".jni", "computerTest_jni.so").getAbsolutePath());
    }
}
