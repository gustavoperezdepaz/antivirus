/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

#include "antivirusTest.c"
extern JNIEnv *javaEnv;

JNIEXPORT void JNICALL Java_antivirusTest_testChangeImage
  (JNIEnv *env, jobject object)
{
    javaEnv = env;
    testChangeImage();
}

JNIEXPORT void JNICALL Java_antivirusTest_testChangeImageAgain
  (JNIEnv *env, jobject object)
{
    javaEnv = env;
    testChangeImageAgain();
}

JNIEXPORT void JNICALL Java_antivirusTest_testChangeImageAgainAgain
  (JNIEnv *env, jobject object)
{
    javaEnv = env;
    testChangeImageAgainAgain();
}

JNIEXPORT void JNICALL Java_antivirusTest_testChangeImageAgainAgainAgain
  (JNIEnv *env, jobject object)
{
    javaEnv = env;
    testChangeImageAgainAgainAgain();
}

JNIEXPORT void JNICALL Java_antivirusTest_testGetImage
  (JNIEnv *env, jobject object)
{
    javaEnv = env;
    testGetImage();
}

JNIEXPORT void JNICALL Java_antivirusTest_testGetRotation
  (JNIEnv *env, jobject object)
{
    javaEnv = env;
    testGetRotation();
}

JNIEXPORT void JNICALL Java_antivirusTest_testGetX
  (JNIEnv *env, jobject object)
{
    javaEnv = env;
    testGetX();
}

JNIEXPORT void JNICALL Java_antivirusTest_testGetY
  (JNIEnv *env, jobject object)
{
    javaEnv = env;
    testGetY();
}

JNIEXPORT void JNICALL Java_antivirusTest_testLookForVirus
  (JNIEnv *env, jobject object)
{
    javaEnv = env;
    testLookForVirus();
}

JNIEXPORT void JNICALL Java_antivirusTest_testLookForVirusBis
  (JNIEnv *env, jobject object)
{
    javaEnv = env;
    testLookForVirusBis();
}

JNIEXPORT void JNICALL Java_antivirusTest_testMove
  (JNIEnv *env, jobject object)
{
    javaEnv = env;
    testMove();
}

JNIEXPORT void JNICALL Java_antivirusTest_testMoveBis
  (JNIEnv *env, jobject object)
{
    javaEnv = env;
    testMoveBis();
}

JNIEXPORT void JNICALL Java_antivirusTest_testMoveBisBis
  (JNIEnv *env, jobject object)
{
    javaEnv = env;
    testMoveBisBis();
}

JNIEXPORT void JNICALL Java_antivirusTest_testTurnLeft
  (JNIEnv *env, jobject object)
{
    javaEnv = env;
    testTurnLeft();
}

JNIEXPORT void JNICALL Java_antivirusTest_testTurnLeft180
  (JNIEnv *env, jobject object)
{
    javaEnv = env;
    testTurnLeft180();
}

JNIEXPORT void JNICALL Java_antivirusTest_testTurnLeft270
  (JNIEnv *env, jobject object)
{
    javaEnv = env;
    testTurnLeft270();
}

JNIEXPORT void JNICALL Java_antivirusTest_testTurnLeft90
  (JNIEnv *env, jobject object)
{
    javaEnv = env;
    testTurnLeft90();
}

JNIEXPORT void JNICALL Java_antivirusTest_testTurnLeftBis
  (JNIEnv *env, jobject object)
{
    javaEnv = env;
    testTurnLeftBis();
}

JNIEXPORT void JNICALL Java_antivirusTest_testTurnLeftBisBis
  (JNIEnv *env, jobject object)
{
    javaEnv = env;
    testTurnLeftBisBis();
}

JNIEXPORT void JNICALL Java_antivirusTest_testTurnRight
  (JNIEnv *env, jobject object)
{
    javaEnv = env;
    testTurnRight();
}

JNIEXPORT void JNICALL Java_antivirusTest_testTurnRight180
  (JNIEnv *env, jobject object)
{
    javaEnv = env;
    testTurnRight180();
}

JNIEXPORT void JNICALL Java_antivirusTest_testTurnRight270
  (JNIEnv *env, jobject object)
{
    javaEnv = env;
    testTurnRight270();
}

JNIEXPORT void JNICALL Java_antivirusTest_testTurnRight90
  (JNIEnv *env, jobject object)
{
    javaEnv = env;
    testTurnRight90();
}

JNIEXPORT void JNICALL Java_antivirusTest_testTurnRightBis
  (JNIEnv *env, jobject object)
{
    javaEnv = env;
    testTurnRightBis();
}

JNIEXPORT void JNICALL Java_antivirusTest_testTurnRightBisBis
  (JNIEnv *env, jobject object)
{
    javaEnv = env;
    testTurnRightBisBis();
}

