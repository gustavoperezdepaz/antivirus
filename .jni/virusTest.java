/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

import org.junit.Test;
import org.junit.runner.RunWith;
import greenfoot.junitUtils.runner.GreenfootRunner;
@RunWith(GreenfootRunner.class)
public class virusTest {
    @Test
    public native void testLookForAllComputer();

    @Test
    public native void testLookForAllComputerBis();

    @Test
    public native void testLookForAllComputerBisBis();

    @Test
    public native void testLookForComputer();

    @Test
    public native void testMove();

    @Test
    public native void testMoveAtEdge();

    @Test
    public native void testTurnLeft();

    @Test
    public native void testTurnLeft180();

    @Test
    public native void testTurnLeft270();

    @Test
    public native void testTurnLeft90();

    @Test
    public native void testTurnLeftAtEdge();

    @Test
    public native void testTurnRight();

    @Test
    public native void testTurnRight180();

    @Test
    public native void testTurnRight270();

    @Test
    public native void testTurnRight90();

    @Test
    public native void testTurnRightAtEdge();

    @Test
    public native void testTwoMoveAtEdge();

    static {
        System.load(new java.io.File(".jni", "virusTest_jni.so").getAbsolutePath());
    }
}
