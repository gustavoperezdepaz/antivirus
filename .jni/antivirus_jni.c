/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "jni_inout.h"
#include "antivirus.c"

#include "greenfoot.h"
extern JNIEnv  *javaEnv;

JNIEXPORT void JNICALL Java_antivirus_start_1
  (JNIEnv *env, jobject object)
{
	javaEnv = env;
    startAntivirus(object);
}

JNIEXPORT void JNICALL Java_antivirus_act_1
  (JNIEnv *env, jobject object)
{
	javaEnv = env;
    actAntivirus(object);
}

