/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

#include "internetTest.c"
extern JNIEnv *javaEnv;

JNIEXPORT void JNICALL Java_internetTest_testStartGame
  (JNIEnv *env, jobject object)
{
    javaEnv = env;
    testStartGame();
}

